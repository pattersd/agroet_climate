import geopandas
import logging
import pyproj
from shapely.geometry import Polygon
from shapely.ops import transform
import time
from agrimet import Agrimet
from azmet import Azmet
from cimis import Cimis
from colorado import Colorado

project_to_4326 = pyproj.Transformer.from_crs(
    "epsg:3857",  # source coordinate system
    "epsg:4326",  # destination coordinate system
    always_xy=True,
)

project_to_3857 = pyproj.Transformer.from_crs(
    "epsg:4326",  # source coordinate system
    "epsg:3857",  # destination coordinate system
    always_xy=True,
)


class Climate(object):
    def __init__(self, aoi_path, logger=None, ignore_cache=False):
        """

        :param aoi_path: Path to the aoi
        :param logger: optional logger
        :param ignore_cache: if True, then don't read from any caches
        """
        self.logger = logger or logging.getLogger()
        self.ignore_cache = ignore_cache
        self.aoi = self.generate_aoi(aoi_path)

    @staticmethod
    def generate_aoi(aoi_path_or_geojson):
        if isinstance(aoi_path_or_geojson, str):
            df = geopandas.read_file(aoi_path_or_geojson)
        else:
            # Check for None in properties
            for f in aoi_path_or_geojson["features"]:
                if f["properties"] is None:
                    f["properties"] = {}
            df = geopandas.GeoDataFrame.from_features(aoi_path_or_geojson["features"])
            df.crs = "epsg:4326"
        df = df.to_crs("epsg:3857")
        aoi = df.unary_union

        # Buffer AOI to make sure we get at least one climate station.
        aoi_buffered = aoi.buffer(25 * 1.6 * 1000)
        aoi_ll = transform(project_to_4326.transform, aoi_buffered)
        return aoi_ll

    def get_climate_objects(self, aoi):
        """Return all climate types that fall in the AOI
        :param aoi Shapely polygon in 4326
        :return List of possible climate extraction objects.
        """
        # ll ul ur lr
        colorado = Polygon([[-109, 20], [-109, 50], [-102, 50], [-102, 20]])
        cali = Polygon([[-124, 20], [-124, 50], [-114, 50], [-114, 20]])
        arizona = Polygon(
            [
                [-114.10018647150422, 37.01614881758243],
                [-109.01959930612749, 36.9523378494808],
                [-109.01959930612749, 31.33702234403667],
                [-111.01668545603974, 31.255110398297205],
                [-114.7712074178747, 32.50321465219598],
                [-114.56351045828384, 36.05338239313292],
                [-114.3238601202944, 36.81176543064409],
                [-114.10018647150422, 37.01614881758243],
            ]
        )
        agrimet = Polygon(
            [
                [-125.87327104762694, 49.681567841812864],
                [-124.40286177114825, 32.95449252779565],
                [-94.57455930543773, 31.084572318659042],
                [-96.67514398612157, 50.08760512766017],
                [-125.87327104762694, 49.681567841812864],
            ]
        )

        climate_extractors = []
        if colorado.intersects(aoi):
            self.logger.debug("Has Colorado climate")
            climate_extractors.append(Colorado())
        if cali.intersects(aoi):
            self.logger.debug("has California climate")
            climate_extractors.append(
                Cimis(logger=self.logger, ignore_cache=self.ignore_cache)
            )
        if arizona.intersects(aoi):
            self.logger.debug("Using Arizona climate")
            climate_extractors.append(Azmet())
        if agrimet.intersects(aoi):
            self.logger.debug("Using Arizona climate")
            climate_extractors.append(Azmet())
        return climate_extractors

    def remove_empty_stations(self, stations_list):
        """
        Remove stations that don't have calculated climate value
        :param stations_list: FeatureCollection
        :return:
        """
        ret = []
        for istation, station in enumerate(stations_list):
            is_okay = True
            for param in ["etr_mm", "etr_hourly_mm"]:
                if param not in station or not station[param]:
                    is_okay = False
                    self.logger.warning(f'Removing station {station["name"]}')
            if is_okay:
                ret.append(station)
        return ret

    @staticmethod
    def convert_to_features(stations):
        # Convert to features
        features = []
        for station in stations:
            if "geometry" in station:
                if "properties" not in station:
                    feature_station = {
                        "type": "Feature",
                        "geometry": station["geometry"],
                        "properties": station,
                    }
                    del feature_station["properties"]["geometry"]
                    features.append(feature_station)
                else:
                    # already a feature
                    features.append(station)
        return features

    def extract_climate(self, hourly_date):
        """
        Extract climate based on aoi location
        :param hourly_date: datetime.datetime
        :return: GeoJSON climate layer
        """
        stations_list = []
        start_time = time.time()

        climate_objs = self.get_climate_objects(self.aoi)
        for climate_obj in climate_objs:
            stations_list += climate_obj.load_climate(self.aoi, hourly_date)
            self.logger.info(
                f"Climate extraction finished in {time.time() - start_time}"
            )

        final_stations_list = self.remove_empty_stations(stations_list)
        features = self.convert_to_features(final_stations_list)
        return {
            "type": "FeatureCollection",
            "features": features,
        }
