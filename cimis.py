import datetime
import json
import logging
import os
import pytz
import requests
from shapely.geometry import Point
import time
from cache_db import Cache


class AgroetException(Exception):
    pass


class Cimis(object):
    stations = []
    api_key = "86630001-8c53-4c0b-b06c-d77f38c5bac1"
    base_url = "https://et.water.ca.gov/api"
    headers = {"content-type": "application/json"}

    def __init__(self, logger=None, ignore_cache=False):
        self.climate_data = {}
        self.logger = logger or logging.getLogger()
        self.ignore_cache = ignore_cache
        self.cache = None
        if not ignore_cache:
            cache = Cache()
            if cache.conn:
                self.cache = cache

        # NOTE: this takes into account daylight savings. To get the real local time, use UTC-8
        #   extraction_date - datetime.timedelta(hours=8)))
        self.local_timezone = pytz.timezone("US/Pacific")

        self.logger.info("Using CIMIS!")

    def query_cache(self, climate_date):
        if self.cache:
            return self.cache.query(climate_date)
        return None

    def get(self, endpoint, params=None, restart_count=0):
        """Query CIMIS API"""
        max_restarts = 5
        timeout_duration = 5

        def restart():
            if restart_count < max_restarts:
                duration = timeout_duration * restart_count
                time.sleep(duration)
                return self.get(endpoint, params, restart_count + 1)
            raise AgroetException(
                "Climate extraction giving up after {0} restarts".format(max_restarts)
            )

        params = params or {}
        params["appKey"] = self.api_key
        req = requests.get(
            os.path.join(self.base_url, endpoint),
            params=params,
            headers=self.headers,
            timeout=300,
        )
        if req.text.find("ERR") > 0:
            self.logger.warning(f"Got cimis connection error: {req.text}")
            return restart()
        try:
            j = json.loads(req.text)
        except json.decoder.JSONDecodeError:
            return restart()

        return j

    def get_stations(self, aoi_buffer_ll=None):
        """Get list of stations that are in the AOI
        :param aoi_buffer_ll Shapely geometry in 4326. If none, get all stations
        :return list of dictionaries with name and coordinates properties in lon, lat order.
        """
        # Remove stations not within 10 miles of the AOI
        ret = None
        if self.cache:
            ret = {
                "Stations": self.cache.query_stations(),
            }
        if not ret:
            ret = self.get("station")

        stations = []
        for station in ret.get("Stations", []):
            if station["IsActive"] == "True":
                x = float(station["HmsLongitude"].split()[2])
                y = float(station["HmsLatitude"].split()[2])
                coordinates = (float(x), float(y))
                geometry = Point(coordinates)
                station["geometry"] = geometry.__geo_interface__
                station["name"] = station["Name"]
                del station["Name"]
                del station["ZipCodes"]
                # removing these because of unicode issues
                del station["HmsLongitude"]
                del station["HmsLatitude"]
                # removing these because of date issues
                del station["ConnectDate"]
                del station["DisconnectDate"]
                if not aoi_buffer_ll or aoi_buffer_ll.contains(geometry):
                    stations.append(station)
        return stations

    @staticmethod
    def get_hourly_records(records):
        return [rec for rec in records if rec["Scope"] == "hourly"]

    def add_climate_data_to_stations(self, stations, station_data_by_id, hourly_date):
        """Iteratate over the station_data and modify it with the climate data.
        :param stations List of climate stations to populate.
        :param station_data_by_id Dictionary of climate date with key station ID
        :param hourly_date datetime.datetime Date to calculate hourly data on.
        """
        for s_id, records in station_data_by_id.items():
            stations_filter = [s for s in stations if s["StationNbr"] == s_id]
            if len(stations_filter):
                station = stations_filter[0]
                for irec, rec in enumerate(records):
                    if rec["Scope"] == "daily":
                        if rec["DayEto"]["Value"] and rec["DayWindRun"]["Value"]:
                            etr = float(rec["DayEto"]["Value"])
                            assert rec["DayEto"]["Unit"] == "(mm)"
                            etr *= 1.2

                            wind_run = float(rec["DayWindRun"]["Value"])
                            assert rec["DayWindRun"]["Unit"] == "(m/s)"
                            station["etr_mm"] = etr
                            # m/s to miles/day
                            # units are actually km/day?
                            station["wrun_mile_day"] = wind_run * 0.621371

                if hourly_date:
                    hourly_records = self.get_hourly_records(records)
                    for irec, rec in enumerate(hourly_records):
                        station_date = get_climate_date(rec["Date"], rec["Hour"])
                        if station_date > hourly_date:
                            # Calculate linear average
                            prev_rec = hourly_records[irec - 1]
                            prev_station_date = get_climate_date(
                                prev_rec["Date"], prev_rec["Hour"]
                            )

                            # Convert to minutes
                            timediff_min = (
                                hourly_date - prev_station_date
                            ).seconds / 60.0
                            if rec["HlyEto"]["Value"] and rec["HlyAirTmp"]["Value"]:
                                try:
                                    etr1 = float(rec["HlyEto"]["Value"])
                                    etr2 = float(prev_rec["HlyEto"]["Value"])
                                    tave1 = float(rec["HlyAirTmp"]["Value"])
                                    tave2 = float(prev_rec["HlyAirTmp"]["Value"])
                                    etrdiff = etr1 - etr2
                                    # Divide by 60 minutes to get the fraction to interpolate by.
                                    #   convert to mm from inches and apply eto from etr factor
                                    etr_mm = etr2 + etrdiff * (timediff_min / 60.0)
                                    tempdiff = tave1 - tave2
                                    tave = tave2 + tempdiff * (timediff_min / 60.0)
                                    station["etr_hourly_mm"] = round(etr_mm * 1.2, 3)
                                    station["tave_hourly_c"] = round(tave, 2)
                                    break
                                except Exception as e:
                                    self.logger.error(e)
                                    self.logger.error(f"failed station row: {station}")
        return stations

    def load_climate(self, aoi, hourly_date):
        """
        Get climate geojson layer with data at daily_date and hour_date
        :param aoi: shapely geometry
        :param hourly_date datetime.datetime
        :return: list of stations
        """
        stations = self.get_stations(aoi)
        station_data_by_id = {}
        if self.cache:
            self.logger.info("Trying cache")
            station_data_by_id = self.query_cache(hourly_date)
            if station_data_by_id:
                self.logger.info("Cache succeeded")

            has_data = False
            for s_id, records in station_data_by_id.items():
                for irec, rec in enumerate(records):
                    if rec["Scope"] == "daily":
                        if rec["DayEto"]["Value"] and rec["DayWindRun"]["Value"]:
                            has_data = True
                        else:
                            self.logger.warning(f"Station {s_id} is missing data")
            if not has_data:
                # Try to query CIMIS using API
                station_data_by_id = {}

        if not station_data_by_id:
            chunk_size = 30
            for lim in range(chunk_size, 10000, chunk_size):
                stations_chunk = stations[lim - chunk_size : lim]
                if not stations_chunk:
                    break
                params = {
                    "startDate": (hourly_date + datetime.timedelta(days=0)).strftime(
                        "%Y-%m-%d"
                    ),
                    "endDate": (hourly_date + datetime.timedelta(days=0)).strftime(
                        "%Y-%m-%d"
                    ),
                    "targets": ",".join([s["StationNbr"] for s in stations_chunk]),
                    "dataItems": ",".join(
                        ["day-eto", "day-wind-run", "hly-eto", "hly-air-tmp"]
                    ),
                    "unitOfMeasure": "M",
                }
                climate = self.get("data", params)
                try:
                    for provider in climate["Data"]["Providers"]:
                        for rec in provider["Records"]:
                            station = [
                                s for s in stations if s["StationNbr"] == rec["Station"]
                            ][0]
                            s_id = station["StationNbr"]
                            # initialize station data
                            if s_id not in station_data_by_id:
                                station_data_by_id[s_id] = []
                            station_data_by_id[s_id].append(rec)
                except Exception as e:
                    raise Exception(
                        "Got an error retrieving CIMIS data ({0}). Please try again shortly".format(
                            e
                        )
                    )

        self.climate_data = station_data_by_id
        return self.add_climate_data_to_stations(
            stations, station_data_by_id, hourly_date
        )

    def test_cimis(self):
        try:
            self.get("station", restart_count=200)
            return True
        except AgroetException:
            return False


def get_climate_date(climate_date, hr=None, convert_to_utc=True):
    ret = datetime.datetime.strptime(climate_date, "%Y-%m-%d")
    if hr:
        if hr[:2] == "24":
            # flip to next day
            dt = datetime.datetime.strptime(climate_date, "%Y-%m-%d")
            ret = dt + datetime.timedelta(days=1)
        else:
            ret = datetime.datetime.strptime(
                "{0} {1}".format(climate_date, hr), "%Y-%m-%d %H%M"
            )
    if convert_to_utc:
        # Don't localize but just add 8 hours to get PST
        use_local_time = False
        utc = pytz.utc
        if use_local_time:
            pacific_tz = pytz.timezone("US/Pacific")
            ret = pacific_tz.localize(ret)
            ret = ret.astimezone(utc)
        else:
            ret += datetime.timedelta(hours=8)
            ret = utc.localize(ret)
    return ret
