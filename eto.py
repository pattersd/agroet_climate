

def eto():
    Z = 12
    df["tk"] = (df.t).astype(float) + 273.16  # Convert temperature from Celsius to Kelvin

    df['es1'] = 0.6108 * np.exp(
        (df.t).astype(float) * 17.27 / ((df.t).astype(float) + 237.3))  # Calculate saturation vapor pressure
    df['VPD1'] = df.es1 - (df.ea).astype(float)  # kpa Calculate vapor pressure deficit (VPD)
    df['DEL'] = (4099 * df.es1) / ((df.t).astype(
        float) + 237.3) ** 2  # Calculate the slope of the saturation vapor pressure vs. air temperature curve at the average hourly air temperature (DEL)
    P1 = 101.3 - 0.0115 * Z + 5.44 * 10 ** 7 * Z ** 2  # Calculate barometric pressure
    df['GAM'] = 0.000646 * (
                1 + 0.000946 * (df.t).astype(float)) * P1  # Calculate psychrometric constant (GAM) (kPa C-1)

    df['W1'] = df.DEL / (df.DEL + df.GAM)  # Calculate weighting function (W)
    df['FU2'] = 0.030 + 0.0576 * (df.w).astype(float)  # Calculate wind function (FU2)
    #   Calculate net radiation (Rn) (Dong et al., 1992)
    df['NR'] = (df.rad).astype(float) / (
                694.5 * (1 - 0.000946 * (df.t).astype(float)))  # Convert Rn from Wm-2 to mm (NR)
    df['ETrh'] = df.W1 * df.NR + (1 - df.W1) * df.VPD1 * df.FU2  # Calculate Hourly Reference Evapotranspiration (RET)

    df['ET24h'] = df['ETrh'].groupby(df['Date']).transform('sum')  # Calculate daily Reference Evapotranspiration (RET)
