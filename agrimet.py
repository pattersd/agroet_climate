import datetime
import json
import os
import pytz
import requests
from shapely.geometry import shape


class Agrimet():
    def __init__(self):
        pass

    def get_climate_layer(self):
        url = 'https://www.usbr.gov/pn/agrimet/agrimetmap/usbr_map.json'
        r = requests.get(url)
        return r.json()

    def load_climate(self, aoi_buffer, extraction_date):
        climate_layer = self.get_climate_layer()

        features = []
        for feat in climate_layer['features']:
            if aoi_buffer.contains(shape(feat['geometry'])):
                feat['properties']['name'] = feat['properties']['Station Name']
                del feat['properties']['Station Name']
                features.append(feat)

        for feat in features:
            climate_data = self.download_climate(feat['properties']['siteid'], 'hourly', extraction_date)
            etr, tave = None, None
            if climate_data:
                station_date, etr1, tave1 = climate_data[0]
                _, etr2, tave2 = climate_data[1]

                # Calculate linear average
                timediff_minutes = (extraction_date - station_date).seconds / 60.0

                if etr1 is not None and etr2 is not None:
                    # Divide by 60 minutes to get the fraction to interpolate by.
                    frac = timediff_minutes / 60.0
                    etrdiff = (etr2 - etr1)
                    etr = etr1 + etrdiff * frac
                    tempdiff = (tave2 - tave1)
                    tave = tave1 + tempdiff * frac

                # Find this station's geometry
                feat['properties'].update({
                    'etr_hourly_mm': etr,
                    'tave_hourly_c': tave,
                })

        # Daily values
        for feat in features:
            climate_data = self.download_climate(feat['properties']['siteid'], 'daily', extraction_date)
            etr, wrun = None, None
            if climate_data:
                station_date, wrun, etr = climate_data[0]
                # windrun is in m/s. Convert to miles/day
                if wrun:
                    wrun = wrun * 24 * 60 * 60 / 1000 * 0.621371
            feat['properties']['etr_mm'] = etr
            feat['properties']['wrun_mile_day'] = wrun
        return features

    @staticmethod
    def download_climate(climate_id, timestep, extraction_date):
        ret = []
        y = extraction_date.year
        m = extraction_date.month
        d = extraction_date.day
        # Read 24 hours worth of data.
        url = f'https://www.usbr.gov/pn-bin/instant.pl?station={climate_id}' \
              f'&year={y}&month={m}&day={d}&year={y}&month={m}&day={d}&pcode=OB&pcode=EA&pcode=UI&pcode=SQ&print_hourly=1'

        r = requests.get(url)
        lines = r.text.split()
        is_reading = False
        for line in lines:
            # Create dicts from row data
            if is_reading:
                tokens = l.split(',')
                if len(tokens) < 3:
                    print('Agrimet: No data for station {0}'.format(climate_id))
                    break
                year, doy, hour = int(tokens[0]), int(tokens[1]), int(tokens[2])
                if timestep == 'hourly':
                    # hour 24 is 0 hour the next day.
                    if hour == 24:
                        doy += 1
                        hour = 0
                    station_date = datetime.datetime(year, 1, 1, hour=hour) + datetime.timedelta(days=doy-1)
                else:
                    station_date = datetime.datetime(year, 1, 1) + datetime.timedelta(days=doy-1)

                mountain = pytz.timezone('US/Mountain')
                station_date = mountain.localize(station_date)
                # Comvert to UTC.
                station_date = station_date.astimezone(pytz.utc)

                if station_date > extraction_date:
                    if timestep == 'hourly':
                        # Add previous hour and this hour.
                        etr, tave = float(prev_tokens[15]), float(prev_tokens[3])
                        ret.append([prev_date, etr, tave])
                        etr, tave = float(tokens[15]), float(tokens[3])
                        ret.append([station_date, etr, tave])
                    else:
                        etr, wrun = float(prev_tokens[24]), float(prev_tokens[18])
                        ret.append([station_date, wrun, etr])
                    break
                prev_date = station_date
                prev_tokens = tokens
            elif l.find('DATE') == 0:
                is_reading = True

        return ret
