import datetime
import logging
import os
import psycopg2
import psycopg2.extras
import pytz
import sys
import time


class Cache(object):
    def __init__(self, db_host=None, db_port=None):
        self.cache_update_date = datetime.datetime.now()

        if not db_host:
            db_host = os.getenv("CACHE_DB_HOST", "localhost")
            db_port = os.getenv("CACHE_DB_PORT", "5434")
        logging.getLogger("cache_db").debug(
            f"Using cache database on {db_host}:{db_port}"
        )
        try:
            self.conn = psycopg2.connect(
                "dbname=agroet user=agroet password='resetnumber1!' host={host} port={port}".format(
                    host=db_host, port=db_port
                )
            )
        except psycopg2.OperationalError:
            logging.getLogger("cache_db").error(
                f"Unable to connect to cache database on {db_host}:{db_port}"
            )
            self.conn = None

    def run_sql(self, sql, args=()):
        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cursor.execute(sql, args)
        self.conn.commit()
        try:
            return cursor.fetchall()
        except psycopg2.ProgrammingError:
            return None

    def get_table_columns(self, table_name):
        ret = self.run_sql(
            "SELECT column_name from information_schema.columns"
            f" where table_schema = 'public' and table_name='{table_name}'"
        )
        return [x["column_name"] for x in ret]

    def get_tables(self):
        ret = self.run_sql("SELECT table_name FROM information_schema.tables")
        tables = []
        for x in ret:
            tables.append(x["table_name"])
        return tables

    def create_tables(self):
        tables = self.get_tables()
        if "stations" not in tables:
            sql = """CREATE TABLE stations (
            id SERIAL PRIMARY KEY,
            station text UNIQUE,
            "Name" text,
            "StationNbr" text,
            "City" text,
            "ZipCodes" text,
            "ConnectDate" Date,
            "DisconnectDate" Date,
            "IsActive" text,
            "IsEtoStation" text,
            "HmsLongitude" text,
            "HmsLatitude" text)
            """
            self.run_sql(sql)
            self.update_stations_table()

        if "daily" not in tables:
            sql = (
                "CREATE TABLE daily "
                "(id SERIAL PRIMARY KEY, "
                "station_id INTEGER REFERENCES stations(id), "
                "dt DATE, "
                "eto REAL, "
                "windrun REAL, "
                "UNIQUE(station_id, dt))"
            )
            self.run_sql(sql)
        if "hourly" not in tables:
            sql = (
                "CREATE TABLE hourly "
                "(id SERIAL PRIMARY KEY, "
                "station_id INTEGER REFERENCES stations(id), "
                "dt TIMESTAMP, "
                "eto REAL, "
                "airtmp REAL, "
                "UNIQUE(station_id, dt))"
            )
            self.run_sql(sql)

    def delete_after(self, dt):
        """Set the range of dates that should be automatically updated"""
        self.cache_update_date = dt

    def update_stations_table(self):
        from cimis import Cimis

        cimis = Cimis()
        ret = cimis.get("station")
        for station in ret["Stations"]:
            self.run_sql(
                """INSERT INTO stations (station, "Name", "StationNbr", "City", "ZipCodes",
                                        "ConnectDate", "DisconnectDate",
                                        "IsActive", "IsEtoStation", "HmsLongitude", "HmsLatitude")
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                (
                    station["StationNbr"],
                    station["Name"],
                    station["StationNbr"],
                    station["City"],
                    station["ZipCodes"],
                    station["ConnectDate"],
                    station["DisconnectDate"],
                    station["IsActive"],
                    station["IsEtoStation"],
                    station["HmsLongitude"],
                    station["HmsLatitude"],
                ),
            )

    def query_stations(self):
        recs = self.run_sql("SELECT * FROM stations")
        return recs

    def has_date(self, dt):
        recs = self.run_sql("SELECT dt FROM daily WHERE dt = %s", (dt.date(),))
        return len(recs) > 0

    def build_cache(self, dt):
        if not self.has_date(dt) or dt >= self.cache_update_date:
            climate_data = self.build_cache_for_date(dt)
            for station_id, station_data in climate_data.items():
                for rec in station_data:
                    self.add_rec(station_id, rec)

    @staticmethod
    def build_cache_for_date(dt):
        # Cache this month's data
        from cimis import Cimis

        cimis = Cimis()
        cimis.load_climate(None, dt)
        return cimis.climate_data

    def get_station_rec(self, station_id):
        ret = self.run_sql(
            'SELECT * FROM stations WHERE "StationNbr"=%s', (station_id,)
        )
        return ret[0]

    def add_rec(self, station_id, rec):
        from cimis import get_climate_date

        station_rec = self.get_station_rec(station_id)
        if rec["Scope"] == "daily":
            date = get_climate_date(rec["Date"])
            self.run_sql(
                """DELETE FROM daily WHERE dt = %s AND station_id = %s""",
                (date.date(), station_rec["id"]),
            )
            try:
                self.run_sql(
                    "INSERT INTO daily (station_id, dt, eto, windrun) VALUES (%s, %s, %s, %s)",
                    (
                        station_rec["id"],
                        date,
                        rec["DayEto"]["Value"],
                        rec["DayWindRun"]["Value"],
                    ),
                )
            except psycopg2.IntegrityError:
                # skip duplicate key exceptions
                self.conn.rollback()
        else:
            date = get_climate_date(rec["Date"], rec["Hour"], convert_to_utc=False)
            self.run_sql(
                """DELETE FROM hourly WHERE dt = %s AND station_id = %s""",
                (date.date(), station_rec["id"]),
            )
            try:
                self.run_sql(
                    "INSERT INTO hourly (station_id, dt, eto, airtmp) VALUES (%s, %s, %s, %s)",
                    (
                        station_rec["id"],
                        date,
                        rec["HlyEto"]["Value"],
                        rec["HlyAirTmp"]["Value"],
                    ),
                )
            except psycopg2.IntegrityError:
                # skip duplicate key exceptions
                self.conn.rollback()

    def query(self, dt):
        """Date must be in UTC"""
        ret = {}
        recs = self.run_sql(
            "SELECT stations.station, daily.* FROM stations, daily WHERE dt = %s AND stations.id = station_id",
            (dt.date(),),
        )
        # Convert to CIMIS format
        for rec in recs:
            station_nbr = rec["station"]
            if station_nbr not in ret:
                ret[station_nbr] = []
            ret[station_nbr].append(
                {
                    "StationNbr": rec["station"],
                    "Date": rec["dt"].strftime("%Y-%m-%d"),
                    "Scope": "daily",
                    "DayEto": {
                        "Value": rec["eto"],
                        "Unit": "(mm)",
                    },
                    "DayWindRun": {
                        "Value": rec["windrun"],
                        "Unit": "(m/s)",
                    },
                }
            )
        # If no daily data, then don't load hourly
        if ret:
            recs = self.run_sql(
                "SELECT stations.station, hourly.* FROM stations, hourly WHERE "
                "EXTRACT(year from dt) = {0} AND "
                "EXTRACT(month from dt) = {1} AND "
                "EXTRACT(day from dt) = {2} AND "
                "stations.id = station_id "
                "ORDER BY dt".format(dt.year, dt.month, dt.day)
            )
            for rec in recs:
                station_nbr = rec["station"]
                # station might have hourly data but not daily
                if station_nbr in ret:
                    # Hourly date is stored in PST. Do not change since the calling program will fix the date.
                    dt = rec["dt"] - datetime.timedelta(hours=0)
                    ret[station_nbr].append(
                        {
                            "Scope": "hourly",
                            "Date": dt.strftime("%Y-%m-%d"),
                            "Hour": "{0:02}00".format(dt.hour),
                            "HlyEto": {"Value": rec["eto"]},
                            "HlyAirTmp": {"Value": rec["airtmp"]},
                        }
                    )
        return ret


if __name__ == "__main__":
    logger = logging.getLogger("cache_db")
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)

    host = None
    port = None
    if len(sys.argv) > 1:
        host = sys.argv[1]
        port = sys.argv[2]
    cache = Cache(host, port)

    if cache.conn:
        cache.create_tables()

        # Test CIMIS connection so we don't delete data that we can't regenerate.
        from cimis import Cimis

        c = Cimis()
        while not c.test_cimis():
            # Test every half hour
            logging.getLogger("cache_db").error("CIMIS is down; pausing update")
            duration = 60 * 30
            time.sleep(duration)
        now = datetime.datetime.now()
        # set for noon UTC so that the updated day of year doesn't change from converting utc to pst.
        now = pytz.utc.localize(datetime.datetime(now.year, now.month, now.day, 12))
        last = pytz.utc.localize(datetime.datetime(now.year - 2, 1, 1))
        logging.getLogger("cache_db").info("Deleting last ten days of cache")
        cache.delete_after(now - datetime.timedelta(days=10))
        while now >= last:
            now -= datetime.timedelta(days=1)
            logging.getLogger("cache_db").info("Processing {0}".format(now))
            cache.build_cache(now)
    logging.getLogger("cache_db").info("finished")
